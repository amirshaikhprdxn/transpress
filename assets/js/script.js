// displays hamburger menu
$('nav').click(function(e) {
	e.preventDefault();
	$this = $(this);
	$this.children('ul').toggleClass('block');
	$this.children('.hamburger').children('.one').toggleClass('ham1');
	$this.children('.hamburger').children('.three').toggleClass('ham3');
	$this.children('.hamburger').children('.two').toggleClass('ham2');
});

var modal_content;
$modal = $('.modal');
$modal_container = $('.modal-container');
// display video modal
$('.video').click(function(){
	$modal.addClass('show-modal');
	modal_content = $("<video></video>");
	modal_content.attr("autoplay", "1");
	modal_content.attr("width", "780");
	modal_content.attr("controls", "true");
	modal_content.attr("src", $('#video-content').attr('src'));
	$modal_container.append(modal_content);
});
// modal closed when clicked on cross
$('.close').click(function(){
	$modal.removeClass('show-modal');
	$modal_container.find('img').remove();
	$modal_container.find('video').remove();
});
// avoid bubbling i.e don't close modal when clicked on modal container
$modal_container.click(function( event ) {
  	event.stopPropagation();
});
// modal closed when clicked on modal 
$modal.click(function(e){
	if ($(this).hasClass('show-modal')) {
		$modal.removeClass('show-modal');
		$modal_container.find('img').remove();
		$modal_container.find('video').remove();
	}
});

// counter functionality starts
var section_top, window_top, section_bottom, window_bottom;
var flag=0;
$(window).scroll(function() {
	section_top = $('.count').parents('.features').offset().top;
	section_bottom = section_top + $('.count').outerHeight();
	window_top = $(window).scrollTop();
	window_bottom = window_top + $(window).height();

	if (flag==0) {
		if (section_bottom >= window_top && section_top <= window_bottom) {
			flag = 1;
			$('.count').each(function() {
				$(this).prop('Counter',0).animate({
					Counter: $(this).text()
				},
				{
					duration: 4000,
					step: function (now) {
						$(this).text(Math.ceil(now));
					}
				});
			});
		}
	}
});

// accordian functionality starts
$('.tab').click(function(e) {
	e.preventDefault();
	$this = $(this);
	$('.tab-content').slideUp();
	// if current tab contains class active hide content else display
	if($this.hasClass("active")) {
		$this.removeClass('active');
		$this.siblings('p').slideUp();
	}	else {
		$('.tab').removeClass('active');
		$this.addClass('active');
		$this.siblings('p').slideDown();
	}
});